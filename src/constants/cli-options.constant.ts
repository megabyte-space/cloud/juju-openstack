export const CLIOptions = {
  append: 'Version 1.x.x',
  options: [
    {
      alias: 'h',
      description: 'Displays help',
      option: 'help',
      type: 'Boolean'
    },
    {
      alias: 'c',
      description: 'Relative path to the configuration file (defaults to ./servr.json)',
      example: 'servr --config ./servr.json',
      option: 'config',
      type: 'String'
    }
  ],
  prepend: 'Usage: servr [options]'
};
