import { default as Pino } from 'pino';

export const pinoLog = Pino({
  prettyPrint: {
    ignore: 'hostname,pid',
    translateTime: 'SYS:HH:MM:ss.l o'
  }
});

/**
 * Pino-powered logger used across the project instead of using
 * console.logs directly
 */
export class Logger {
  /** Debug log level */
  public static debug(...args): void {
    pinoLog.debug(...args);
  }

  /** Error log level */
  public static error(...args): void {
    pinoLog.error(...args);
  }

  /** Info log level */
  public static info(...args): void {
    pinoLog.info(...args);
  }

  /** Alias for info log level */
  public static log(...args): void {
    pinoLog.info(...args);
  }

  /** Warn log level */
  public static warn(...args): void {
    pinoLog.warn(...args);
  }
}
