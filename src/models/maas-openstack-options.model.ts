import { IsString } from 'class-validator';
import { execSync } from '../lib/util';

/**
 * A class that is used to validate the configuration necessary
 * to generate Juju configuration files.
 */
export class MAASOpenStackOptions {
  @IsString()
  /** The MAAS user with admin priviledges */
  public readonly admin: string;
  @IsString()
  /** The DNS domain to add the Juju and MAAS entries to for proxying */
  public readonly domain: string = 'maas';
  @IsString()
  /** The URL or domain name of the MAAS Web UI */
  public readonly hostname: string = 'localhost';
  @IsString()
  /** The IP address of the host */
  public readonly ip: string;
  @IsString()
  /** The API key for MAAS */
  public readonly key?: string;
  @IsString()
  /** The name of the model to deploy Kubernetes to */
  public readonly model?: string = 'kuber';
  @IsString()
  /** The name of the MAAS cloud */
  public readonly name: string;

  constructor(options: MAASOpenStackOptions) {
    this.admin = options.admin;
    this.domain = options.domain ? options.domain : this.domain;
    this.hostname = options.hostname ? options.hostname : this.hostname;
    this.ip = options.ip;
    this.model = options.model ? options.model : this.model;
    this.name = options.name;
    this.key = execSync('sudo maas-region apikey --username=' + this.admin).toString();
  }
}
