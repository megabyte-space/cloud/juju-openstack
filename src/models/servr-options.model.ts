import { IsOptional, ValidateNested } from 'class-validator';
import { MAASOpenStackOptions } from './maas-openstack-options.model';

/**
 * A bundle of all the different configurations retrieved from a
 * configuration file.
 */
export class ServrOptions {
  @IsOptional()
  @ValidateNested()
  /** Options used to generate Juju files */
  public readonly maas?: MAASOpenStackOptions;

  constructor(options: ServrOptions) {
    if (options.maas) {
      this.maas = new MAASOpenStackOptions(options.maas);
    }
  }
}
