# @woogie/servr

A collection of tools that makes it easier to:
* Deploy a MAAS Kubernetes Cloud powered by OpenStack and Rancher
* Set up nginx with the best configurations
* Tighten up the security of SSH
* Configure the firewall
